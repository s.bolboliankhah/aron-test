import http from '../services/http';

const login = async (data) => {
  return await http({
    url: "/auth/login",
    method: 'POST',
    data
  });
}
const register = async (data) => {
  return await http({
    url: "/auth/register",
    method: 'POST',
    data
  });
}
const verify = async (data) => {
  return await http({
    url: "/auth/verify-otp",
    method: 'POST',
    data
  });
}
const reSendCode = async (data) => {
  return await http({
    url: "/auth/resend-otp",
    method: 'POST',
    data
  });
}
export { login, register, verify, reSendCode } 