import { lightTheme, darkTheme } from "./default";
import { createTheme } from "@mui/material";

const overrides = {
  typography: {
    h1: {
      fontWeight: 700,
      fontSize: "4rem",
      lineHeight: 1.167,
    },
    h2: {
      fontWeight: 700,
      fontSize: "3rem",
      lineHeight: 1.167,
    },
    h3: {
      fontWeight: 600,
      fontSize: "2rem",
      lineHeight: 1.167,
    },
    h4: {
      fontWeight: 600,
      fontSize: "1.5rem",
      lineHeight: 1.435,
    },
    h5: {
      fontWeight: 700,
      fontSize: "1.2rem",
      lineHeight: 1.6,
    },
    h6: {
      fontWeight: 600,
      fontSize: "1rem",
      lineHeight: 1.6,
    },
    body1: {
      fontWeight: 500,
      fontSize: "0.9rem",
      lineHeight: 1.5,
    },
    body2: {
      fontWeight: 500,
      fontSize: "0.8rem",
      lineHeight: 1.8,
    },
    body3: {
      fontWeight: 400,
      fontSize: "0.9rem",
      lineHeight: 1.8,
    },
    body4: {
      fontWeight: 400,
      fontSize: "0.8rem",
      lineHeight: 2.5,
    },
    fontFamily: `"Inter", "Inter-Medium"`,
    fontWeightLight: 300,
    fontWeightRegular: 400,
    fontWeightMedium: 500,
    // fontWeightBold: 700,
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 960,
      lg: 1280,
      xl: 1920,
    },
  },
};

export default {
  main: createTheme({ ...lightTheme, ...overrides }),
  dark: createTheme({ ...darkTheme, ...overrides }),
};
