export const lightTheme = {
  palette: {
    mode: "light",
    common: {
      white: "#ffffff",
      black: "#000000",
    },
    primary: {
      main: "#1B84ff",   //ok
      light: "#E9F3FF",  //ok
      dark: "#071437",   //ok
    },
    secondary: {
      main: "#17C653",
      light: "#F8285A",
      dark: "#343946" //ok
    },
    text: {
      primary: "#071437",
      secondary: "#99A1B7",
      disabled: "#b6b9c8",
      primaryChannel: "#071437", //ok
      secondaryChannel: "#ffffff",
    },
    background: {
      paper: "#ffffff", //ok
      default: "#fbfbfb", //ok
    },
  },
};
export const darkTheme = {
  palette: {
    mode: "light",
    common: {
      white: "#ffffff",
      black: "#000000",
    },
    primary: {
      main: "#1B84ff",   //ok
      light: "#E9F3FF",  //ok
      dark: "#071437",   //ok
    },
    secondary: {
      main: "#17C653",
      light: "#F8285A",
      dark: "#343946" //ok
    },
    text: {
      primary: "#071437",
      secondary: "#99A1B7",
      disabled: "#b6b9c8",
      primaryChannel: "#ffffff", //ok
      secondaryChannel: "#071437",
    },
    background: {
      paper: "#071437", //ok 
      default: "#343946", //ok
    },
  },
};
