import Router from "./router"
import { ThemeProvider, CssBaseline } from "@mui/material";
import Theme from "./assets/theme";
import './App.css';
import { useLayoutState } from "./context/LayoutContext";
import rtlPlugin from "stylis-plugin-rtl";
import { CacheProvider } from "@emotion/react";
import createCache from "@emotion/cache";
import { useTranslation } from "react-i18next";


const cacheRtl = {
  key: "muirtl",
  stylisPlugins: [rtlPlugin]
};
const cacheLtr = {
  key: "muiltr"
};

function App() {
  const { isDark } = useLayoutState()
  const { i18n: { dir, language } } = useTranslation();

  return (
    <CacheProvider value={createCache(dir(language) === "rtl" ? cacheRtl : cacheLtr)}>
      <ThemeProvider theme={isDark ? Theme.dark : Theme.main}>
        <CssBaseline />
        <Router />
      </ThemeProvider>
    </CacheProvider>
  );
}

export default App;
