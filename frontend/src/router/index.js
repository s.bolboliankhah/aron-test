import {
  Navigate,
  createBrowserRouter,
  RouterProvider,
} from 'react-router-dom'

import Login from '../pages/login'
import Register from '../pages/register'
import Dashboard from '../pages/dashboard'
import NotFound from '../pages/404'
import RouterGuard from './RouterGuard'

export const routes = [
  { path: '/', element: <Navigate replace to="/app/dashboard" /> },
  { path: 'app', element: <Navigate replace to="/app/dashboard" /> },
  {
    path: '/app',
    element: <RouterGuard />,
    children: [
      { path: 'dashboard', element: <Dashboard /> },
    ],
  },
  { path: 'login', element: <Login /> },
  { path: 'register', element: <Register /> },
  { path: '*', element: <NotFound /> },
]

export const router = createBrowserRouter(routes)

const Router = () => {
  return <RouterProvider router={router} />
}

export default Router
