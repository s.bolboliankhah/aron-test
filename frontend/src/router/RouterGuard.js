import { Navigate } from 'react-router-dom'
import Layout from '../components/layout'
import { useUserState } from '../context/UserContext'

function RouterGuard() {
  const { token } = useUserState()

  return !!token ? <Layout /> : <Navigate replace to="/login" />

}

export default RouterGuard
