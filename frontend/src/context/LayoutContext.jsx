import { useState, createContext, useContext, useEffect } from "react";
import { useTranslation } from "react-i18next";

const LayoutStateContext = createContext();

function LayoutProvider({ children }) {
  const [isSidebarOpened, setIsSidebarOpened] = useState(false);
  const [isDark, setIsDark] = useState(false);

  const {
    i18n: { dir, language },
  } = useTranslation();

  useEffect(() => {
    const open = localStorage.getItem("isSidebarOpened") || false
    const dark = localStorage.getItem("isDark") || false
    setIsSidebarOpened(open === "true")
    setIsDark(dark === "true")
  }, [])

  const updateLayout = (type, isTrue) => {
    switch (type) {
      case "theme":
        localStorage.setItem("isDark", JSON.stringify(!!isTrue))
        setIsDark(!!isTrue)
        break;
      case "sidebar":
        localStorage.setItem("isSidebarOpened", JSON.stringify(!!isTrue))
        setIsSidebarOpened(!!isTrue)
        break;
      default:
        break;
    }
  }
  return (
    <div dir={dir(language)}>
      <LayoutStateContext.Provider
        value={{ isSidebarOpened, isDark, updateLayout }}
      >
        {children}
      </LayoutStateContext.Provider>
    </div>
  );
}

function useLayoutState() {
  const context = useContext(LayoutStateContext);
  if (context === undefined) {
    throw new Error("useUserState must be used within a UserProvider");
  }
  return context;
}

export { LayoutProvider, useLayoutState };
