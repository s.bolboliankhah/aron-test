import { useState, createContext, useContext, useEffect, useReducer } from "react";

const UserStateContext = createContext();
const UserDispatchContext = createContext();

function userReducer(state, action) {
    switch (action.type) {
        case 'LOGIN_SUCCESS':
            return {
                ...state,
                fullName: action.payload.fullName,
                token: action.payload.token,
            };
        case 'SIGN_OUT_SUCCESS':
            return { ...state, token: "", fullName: ''};
        case 'LOGIN_FAILURE':
            return { ...state, token: "", fullName: ''};
        default: {
            throw new Error(`Unhandled action type: ${action.type}`);
        }
    }
}

function UserProvider({ children }) {
    const [state, dispatch] = useReducer(userReducer, {
        token: getData('token'),
        fullName: getData('fullName')
    });

    return (
        <UserStateContext.Provider value={state}>
            <UserDispatchContext.Provider value={dispatch}>{children}</UserDispatchContext.Provider>
        </UserStateContext.Provider>
    );
}

function useUserState() {
    const context = useContext(UserStateContext);
    if (context === undefined) {
        throw new Error("useUserState must be used within a UserProvider");
    }
    return context;
}
function useUserDispatch() {
    const context = useContext(UserDispatchContext);
    if (context === undefined) {
        throw new Error('useUserDispatch must be used within a UserProvider');                  
    }
    return context;
}
export { UserProvider, useUserState, useUserDispatch, clearUserData, storeUserData, removeStorageData };

const storeUserData = ({ token, roles, fullName, dispatch }) => {
    token = encodeURIComponent(token)
    storeData('token', token);
    storeData('fullName', fullName);
    // storeData('roles', roles);
    dispatch({
        type: 'LOGIN_SUCCESS',
        payload: {
            fullName,
            // roles,
            token,
        },
    });
}
const clearUserData = ({ dispatch }) => {
    dispatch({
        type: 'SIGN_OUT_SUCCESS',
        payload: {},
    });
    removeStorageData()
}

const removeStorageData = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("fullName");
    localStorage.removeItem("roles");
    window.location.href = '/login';
}
function getData(key) {
    const strData = localStorage.getItem(key);
    const jsonData = JSON.parse(JSON.stringify(strData));
    return jsonData || "";
}
function storeData(key, data) {
    if (typeof data == "string") {
        localStorage.setItem(key, data);
    } else {
        localStorage.setItem(key, JSON.stringify(data));
    }
}