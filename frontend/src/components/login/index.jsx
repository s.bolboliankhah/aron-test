import { Box, Button } from '@mui/material';
import { useTranslation } from 'react-i18next';
import LoginForm from './LoginForm';
import MiddleText from '../share/text/MiddleText';
import SmallText from '../share/text/SmallText';
import { useNavigate } from 'react-router-dom';

const Login = () => {

  const { t } = useTranslation()
  const navigate = useNavigate()

  return (
    <>
      <Box
        // height={{ md: '500px', xs: '50vh' }}
        width={{
          md: '800px',
          sm: '600px',
          xs: '400px',
        }}
        display="grid"
        alignItems="center"
        bgcolor="text.primaryChannel"
        borderRadius="50px"
        position="relative"
        py="10%"
      >
        <MiddleText text={t("login.titleForm")} />
        <LoginForm />
        <Button sx={{ mt: 2 }} onClick={() => navigate("/register")}>
          <SmallText text={t("register.no")} />
        </Button>
      </Box>
    </>
  );
};

export default Login;
