import { Box } from '@mui/material';
import { Form, Formik } from 'formik';
import * as Yup from 'yup';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import SubmitButtonFill from "../share/form/SubmitButtonFill"
import { useUserDispatch, storeUserData } from '../../context/UserContext';
import { useNavigate } from 'react-router-dom';
import { login } from '../../apis/auth';
import { useSnackbar } from 'notistack';
import CustomInputFill from '../share/form/CustomInputFill';

const LoginForm = () => {
    const [isLoading, setIsLoading] = useState(false);
    const { enqueueSnackbar } = useSnackbar();
    const dispatch = useUserDispatch()
    const navigate = useNavigate()

    const { t } = useTranslation()
    const list = [
        {
            tag: (
                <CustomInputFill
                    name="mobile"
                    text={t("login.mobile")}
                    placeholder={t("login.mobile")}
                />
            ),
        },
        {
            tag: (
                <CustomInputFill
                    name="password"
                    text={t("login.password")}
                    placeholder={t("login.password")}
                    type="password"
                />
            ),
        },
    ];
    const Validation_Schema = Yup.object({
        mobile: Yup.string().matches(/^09([0-9]{9}$)/, t("all.mobileFormat")).required(t("all.required")),
        password: Yup.string().required(t("all.required")),
    });

    const initialValuesCreate = {
        mobile: '',
        password: ''
    };

    const handleSubmit = async (values) => {
        setIsLoading(true);
        const res = await login(values)
        if (res.success) {
            storeUserData({
                dispatch,
                token: res.dataBody.token,
                roles: [], // to do
                fullName: `${res.dataBody.user.fname} ${res.dataBody.user.lname}`
            })
            navigate("/")
        } else {
            res.messages.map(m => enqueueSnackbar(m.message, { variant: res.variant }))
        }
        setIsLoading(false);
    };

    return (
        <Formik
            initialValues={initialValuesCreate}
            validationSchema={Validation_Schema}
            onSubmit={handleSubmit}

        >
            <Form>
                {list.map((el, index) => (
                    <Box key={index} width="60%" mx="auto">
                        {el.tag}
                    </Box>
                ))}
                <Box
                    width="45%"
                    textAlign="center"
                    mx="auto"
                    mt="15px"
                >
                    <SubmitButtonFill text={t("login.title")} isLoading={isLoading} />
                </Box>
            </Form>
        </Formik>
    );
};

export default LoginForm;
