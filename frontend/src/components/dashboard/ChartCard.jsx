import { Card } from '@mui/material';
import Circle from '../share/charts/Circle';
import { useTranslation } from 'react-i18next';
import MiddleTextReversColor from '../share/text/MiddleTextReversColor';

export default function ChartCard() {

  const { t } = useTranslation()
  return (
    <Card sx={{}}>
      <MiddleTextReversColor text={t("chart.title")} />
      <Circle />
    </Card>
  );
}