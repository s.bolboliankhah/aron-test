import { Grid } from "@mui/material";
import ChartCard from "./ChartCard";

const Dashboard = () => {
    return (
        <Grid spacing={2} container>
            <Grid md={4} my={2} item>
                <ChartCard />
            </Grid>
            {/* <Grid md={4} my={2} item>
            </Grid>
            <Grid md={4} my={2} item>
            </Grid> */}
        </Grid>
    );
};

export default Dashboard;
