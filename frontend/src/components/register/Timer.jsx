import { useState, useEffect } from 'react';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import AvTimerIcon from '@mui/icons-material/AvTimer';
import { Box } from '@mui/material';

const Timer = ({ seconds, setSeconds }) => {

    useEffect(() => {
        let timer;

        if (seconds > 0) {
            timer = setInterval(() => {
                setSeconds(prevSeconds => prevSeconds - 1);
            }, 1000);
        }

        return () => {
            clearInterval(timer);
        };
    }, [seconds]);

    return (
        <div>
            {
                !!seconds &&
                <Box display="flex" alignItems="center" justifyContent="center" mt={3} >
                    <Typography color="common.white" variant="h6" >
                        {seconds}
                    </Typography>
                    <AvTimerIcon fontSize='large' sx={{ color: "common.white" }} />
                </Box>
            }
        </div>
    );
};

export default Timer;