import { Box } from '@mui/material';
import { Form, Formik } from 'formik';
import * as Yup from 'yup';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import SubmitButtonFill from "../share/form/SubmitButtonFill"
import { useUserDispatch, storeUserData } from '../../context/UserContext';
import { useNavigate } from 'react-router-dom';
import { reSendCode, verify } from '../../apis/auth';
import { useSnackbar } from 'notistack';
import CustomInputFill from '../share/form/CustomInputFill';
import Timer from "./Timer"
import CustomButton from '../share/form/CustomButton';

const VerifyOTP = ({ mobile, remainingSecond }) => {
    const [isLoading, setIsLoading] = useState(false);
    const [seconds, setSeconds] = useState(remainingSecond);
    const { enqueueSnackbar } = useSnackbar();
    const dispatch = useUserDispatch()
    const navigate = useNavigate()

    const { t } = useTranslation()
    const list = [
        {
            tag: (
                <CustomInputFill
                    disabled={!seconds}
                    name="code"
                    text={t("register.code")}
                    placeholder={t("register.code")}
                    type="number"
                />
            ),
        },
    ];
    const Validation_Schema = Yup.object({
        code: Yup.number().required(t("all.required"))
    });

    const initialValuesCreate = {
        code: ''
    };

    const handleSubmit = async (values) => {
        setIsLoading(true);
        values.mobile = mobile
        const res = await verify(values)
        if (res.success) {
            storeUserData({
                dispatch,
                token: res.dataBody.token,
                roles: [], // to do
                fullName: `${res.dataBody.user.fname} ${res.dataBody.user.lname}`
            })
            navigate("/")
        } else {
            res.messages.map(m => enqueueSnackbar(m.message, { variant: res.variant }))
        }
        setIsLoading(false);
    };

    const handleReSendCode = async (values) => {
        setIsLoading(true);
        values.mobile = mobile
        const res = await reSendCode({ mobile: values.mobile })
        res.messages.map(m => enqueueSnackbar(m.message, { variant: res.variant }))
        if (res.success) {
            setSeconds(res.dataBody.remainingSecond)
        }
        setIsLoading(false);
    };
    return (
        <>

            <Formik
                initialValues={initialValuesCreate}
                validationSchema={Validation_Schema}
                onSubmit={handleSubmit}

            >
                <Form>
                    {list.map((el, index) => (
                        <Box key={index} width="60%" mx="auto">
                            {el.tag}
                        </Box>
                    ))}
                    <Box
                        width="45%"
                        textAlign="center"
                        mx="auto"
                        mt="15px"
                    >
                        {
                            seconds
                                ? <SubmitButtonFill text={t("all.confirm")} isLoading={isLoading} />
                                : <CustomButton text={t("register.resendCode")} isLoading={isLoading} handleSubmit={handleReSendCode} />
                        }
                    </Box>
                </Form>
            </Formik>
            <Timer seconds={seconds} setSeconds={setSeconds} />
        </>
    );
};

export default VerifyOTP;
