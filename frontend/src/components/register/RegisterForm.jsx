import { Box } from '@mui/material';
import { Form, Formik } from 'formik';
import * as Yup from 'yup';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import SubmitButtonFill from "../share/form/SubmitButtonFill"
import { register } from '../../apis/auth';
import { useSnackbar } from 'notistack';
import CustomInputFill from '../share/form/CustomInputFill';

const RegisterForm = ({ setMobile, setRemainingSecond }) => {
    const [isLoading, setIsLoading] = useState(false);
    const { enqueueSnackbar } = useSnackbar();

    const { t } = useTranslation()
    const list = [
        {
            tag: (
                <CustomInputFill
                    name="fname"
                    text={t("register.fname")}
                    placeholder={t("register.fname")}
                />
            ),
        },
        {
            tag: (
                <CustomInputFill
                    name="lname"
                    text={t("register.lname")}
                    placeholder={t("register.lname")}
                />
            ),
        },
        {
            tag: (
                <CustomInputFill
                    name="mobile"
                    text={t("register.mobile")}
                    placeholder={t("register.mobile")}
                    type="number"
                />
            ),
        },
        {
            tag: (
                <CustomInputFill
                    name="email"
                    text={t("register.email")}
                    placeholder={t("register.email")}
                    type="email"
                />
            ),
        },
        {
            tag: (
                <CustomInputFill
                    name="password"
                    text={t("login.password")}
                    placeholder={t("login.password")}
                    type="password"
                />
            ),
        },
    ];
    const Validation_Schema = Yup.object({
        mobile: Yup.string().matches(/^09([0-9]{9}$)/, t("all.mobileFormat")).required(t("all.required")),
        email: Yup.string().email().required(t("all.required")),
        fname: Yup.string().required(t("all.required")),
        lname: Yup.string().required(t("all.required")),
        password: Yup.string().required(t("all.required")),
    });

    const initialValuesCreate = {
        mobile: '',
        fname: '',
        lname: '',
        email: '',
        password: ''
    };

    const handleSubmit = async (values) => {
        setIsLoading(true);
        const res = await register(values)
        res.messages.map(m => enqueueSnackbar(m.message, { variant: res.variant }))
        if (res.success) {
            setRemainingSecond(res.dataBody.remainingSecond)
            setMobile(values.mobile)
        }
        setIsLoading(false);
    };

    return (
        <Formik
            initialValues={initialValuesCreate}
            validationSchema={Validation_Schema}
            onSubmit={handleSubmit}
        >
            <Form>
                {list.map((el, index) => (
                    <Box key={index} width="60%" mx="auto">
                        {el.tag}
                    </Box>
                ))}
                <Box
                    width="45%"
                    textAlign="center"
                    mx="auto"
                    mt="15px"
                >
                    <SubmitButtonFill text={t("register.title")} isLoading={isLoading} />
                </Box>
            </Form>
        </Formik>
    );
};

export default RegisterForm;
