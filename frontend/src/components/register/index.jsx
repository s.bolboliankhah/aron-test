import { Box, Button } from '@mui/material';
import { useTranslation } from 'react-i18next';
import RegisterForm from './RegisterForm';
import MiddleText from '../share/text/MiddleText';
import SmallText from '../share/text/SmallText';
import { useNavigate } from 'react-router-dom';
import { useState } from 'react';
import VerifyOTP from './VerifyOTP';

const Register = () => {

  const { t } = useTranslation()
  const navigate = useNavigate()
  const [mobile, setMobile] = useState("")
  const [remainingSecond, setRemainingSecond] = useState(0)

  const Form = () => {
    return (
      <>
        <MiddleText text={t("register.titleForm")} />
        <RegisterForm setRemainingSecond={setRemainingSecond} setMobile={setMobile} />
        <Button sx={{ mt: 2 }} onClick={() => navigate("/login")}>
          <SmallText text={t("login.account")} />
        </Button>
      </>)
  }
  const SetOTP = () => {
    return (
      <>
        <MiddleText text={mobile} />
        <VerifyOTP mobile={mobile} remainingSecond={remainingSecond}/>
      </>)
  }
  return (
    <>
      <Box
        // height={{ md: '500px', xs: '50vh' }}
        width={{
          md: '800px',
          sm: '600px',
          xs: '400px',
        }}
        display="grid"
        alignItems="center"
        bgcolor="text.primaryChannel"
        borderRadius="50px"
        position="relative"
        py="10%"
      >
        {
          !mobile ? <Form /> : <SetOTP />
        }
      </Box>
    </>
  );
};

export default Register;
