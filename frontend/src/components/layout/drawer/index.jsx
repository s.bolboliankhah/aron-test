import { useState } from "react"
import { styled } from '@mui/material/styles';
import MuiDrawer from '@mui/material/Drawer';
import { List, Divider, IconButton, ListItem, ListItemButton, ListItemText, ListItemIcon } from '@mui/material';
import { Home as HomeIcon, Task as TaskIcon, Diversity2 as GroupIcon } from '@mui/icons-material';
import Header from '../header';
import { useLayoutState } from '../../../context/LayoutContext';
import { useTranslation } from 'react-i18next';
import { useLocation, useNavigate } from 'react-router-dom';
import BackButton from "../../utils/BackButton";

const drawerWidth = 240;

const openedMixin = (theme) => ({
  backgroundColor: theme.palette.background.paper,
  width: drawerWidth,
  transition: theme.transitions.create('width', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen,
  }),
  overflowX: 'hidden',
});

const closedMixin = (theme) => ({
  backgroundColor: theme.palette.background.paper,
  transition: theme.transitions.create('width', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: 'hidden',
  width: `calc(${theme.spacing(7)} + 1px)`,
  [theme.breakpoints.up('sm')]: {
    width: `calc(${theme.spacing(8)} + 1px)`,
  },
});

const Drawer = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== 'open' })(
  ({ theme, open }) => ({
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
    boxSizing: 'border-box',
    ...(open && {
      ...openedMixin(theme),
      '& .MuiDrawer-paper': openedMixin(theme),

    }),
    ...(!open && {
      ...closedMixin(theme),
      '& .MuiDrawer-paper': closedMixin(theme),
    }),
  }),
);

const list = [
  {
    id: 1,
    title: "drawer.home",
    icon: <HomeIcon />,
    link: "/app/dashboard"
  },
  {
    id: 2,
    title: "drawer.tasks",
    icon: <TaskIcon />,
    link: "/app"
  },
  {
    id: 3,
    title: "drawer.groups",
    icon: <GroupIcon />,
    link: "/app"
  }
]
export default function MiniDrawer({ DrawerHeader }) {
  const location = useLocation();
  const { isSidebarOpened, updateLayout } = useLayoutState();
  const [selected, setSelected] = useState(list.find(el => location.pathname.includes(el.link))?.id || 1)
  const { t } = useTranslation()
  const navigate = useNavigate()

  return (
    <>
      <Header />
      <Drawer variant="permanent" open={isSidebarOpened}>
        <DrawerHeader>
          <BackButton handleClick={() => updateLayout("sidebar", false)} />
        </DrawerHeader>
        <Divider />
        <List>
          {list.map((el, index) => (
            <ListItem key={index} disablePadding sx={{ display: 'block' }}>
              <ListItemButton
                onClick={() => {
                  setSelected(el.id)
                  navigate(el.link)
                }}
                sx={{
                  minHeight: 48,
                  justifyContent: isSidebarOpened ? 'initial' : 'center',
                  px: 2.5,
                  borderRadius: "30px",
                  width: "90%",
                  mx: "auto"
                }}
              >
                <ListItemIcon
                  sx={{
                    minWidth: 0,
                    mr: isSidebarOpened ? 3 : 'auto',
                    justifyContent: 'center',
                  }}
                >
                  <IconButton sx={{ color: selected == el.id ? "primary.main" : "text.primaryChannel" }} >{el.icon}</IconButton>
                </ListItemIcon>
                {isSidebarOpened && <ListItemText primary={t(el.title)} sx={{ opacity: 1, color: selected == el.id ? "primary.main" : "text.primaryChannel" }} />}
              </ListItemButton>
            </ListItem>
          ))}
        </List>
      </Drawer>
    </>
  );
}