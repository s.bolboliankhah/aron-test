import { styled } from '@mui/material/styles';
import { IconButton, Typography, CssBaseline, Toolbar, AppBar as MuiAppBar, Box } from '@mui/material';
import {
  DarkMode as DarkModeIcon,
  LightMode as LightModeIcon,
  Menu as MenuIcon
} from '@mui/icons-material';
import { useLayoutState } from '../../../context/LayoutContext';
import Logout from '../../share/Logout';
import { useUserState } from '../../../context/UserContext';
import SwitchLanguage from '../../utils/SwitchLanguage';

const drawerWidth = 240;

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(['width', 'margin'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));


export default function Header() {
  const { isSidebarOpened, updateLayout, isDark } = useLayoutState();
  const { fullName } = useUserState()

  return (
    <>
      <CssBaseline />
      <AppBar position="fixed" open={isSidebarOpened} sx={{ backgroundColor: "primary.dark" }} >
        <Toolbar sx={{ display: "flex", justifyContent: "space-between", alignItems: 'center' }}>
          <Box display="flex" alignItems="center" >
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={() => updateLayout("sidebar", true)}
              edge="start"
              sx={{
                marginRight: 5,
                ...(isSidebarOpened && { display: 'none' }),
              }}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" noWrap component="div">
              {fullName}
            </Typography>
          </Box>
          <Box display="flex" alignItems="center" >
            <SwitchLanguage />
            <IconButton onClick={() => updateLayout("theme", !isDark)} sx={{ color: "common.white" }}>
              {isDark ? <DarkModeIcon /> : <LightModeIcon />}
            </IconButton>
            <Logout />
          </Box>
        </Toolbar>
      </AppBar>
    </>
  );
}