import { Box, styled } from '@mui/material';
import Drawer from './drawer';
import { Outlet } from 'react-router-dom';
import CustomSnackbar from '../share/CustomSnackbar';

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'flex-end',
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
}));

export default function MiniDrawer() {
  return (
    <CustomSnackbar>
      <Box sx={{ display: 'flex' }}>
        <Drawer DrawerHeader={DrawerHeader} />
        <Box component="main" sx={{ flexGrow: 1, p: 3, overflowX: "hidden" }}>
          <DrawerHeader />
          <Outlet />
        </Box>
      </Box>
    </CustomSnackbar>
  );
}