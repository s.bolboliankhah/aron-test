import { Clear as ClearIcon } from '@mui/icons-material';
import { IconButton } from '@mui/material';

const CancelButton = ({ handleClick = () => { } }) => {
    return (
        <IconButton onClick={handleClick} sx={{ bgcolor: "primary.light", mx: "5px", "&:hover": { backgroundColor: "primary.light" } }} >
            <ClearIcon />
        </IconButton>
    )
}

export default CancelButton;
