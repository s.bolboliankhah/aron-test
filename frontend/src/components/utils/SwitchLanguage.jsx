import { Button } from "@mui/material";
import { useTranslation } from "react-i18next";

const SwitchLanguage = ({ }) => {
  const { i18n } = useTranslation();
  const newLang = i18n.language == "fa" ? "en" : "fa";
  return (
    <Button
      onClick={() => {
        i18n.changeLanguage(newLang, () => {
          window.location.reload();
        });
        localStorage.setItem("lang", newLang);
      }}
    >
      {newLang.toUpperCase()}
    </Button>
  );
};

export default SwitchLanguage;
