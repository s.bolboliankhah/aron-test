import React, { useRef, useEffect } from "react";

/**
 * Hook that alerts clicks outside of the passed ref
 */
function useOutsideAlerter(ref, handleOutClick) {
    useEffect(() => {
        function handleClickOutside(event) {
            if (ref.current && !ref.current.contains(event.target)) {
                handleOutClick()
            }
        }
        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, [ref]);
}

/**
 * Component that alerts if you click outside of it
 */
export default function OutsideAlerter({ handleOutClick, children }) {
    const wrapperRef = useRef(null);
    useOutsideAlerter(wrapperRef, handleOutClick);

    return <div>
        <div ref={wrapperRef}>
            {children}
        </div>
    </div>;
}