import { ChevronLeft as ChevronLeftIcon } from '@mui/icons-material';
import { IconButton } from '@mui/material';

const BackButton = ({ handleClick = () => { }, sx = {} }) => {
    return (
        <IconButton onClick={handleClick} sx={{ ...sx, bgcolor: "primary.light", mx: "5px", "&:hover": { backgroundColor: "primary.light" } }} >
            <ChevronLeftIcon />
        </IconButton>
    )
}

export default BackButton;
