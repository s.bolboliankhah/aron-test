import { Check as CheckIcon } from '@mui/icons-material';
import { IconButton } from '@mui/material';

const CheckButton = ({ handleClick = () => { } }) => {
    return (
        <IconButton onClick={handleClick} sx={{ bgcolor: "primary.light", mx: "5px", "&:hover": { backgroundColor: "primary.light" } }} >
            <CheckIcon />
        </IconButton>
    )
}

export default CheckButton;
