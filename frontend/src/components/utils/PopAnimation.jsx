import { styled } from '@mui/system';
import PropTypes from 'prop-types';
import { useCallback, useEffect } from 'react';

function Animated(props) {
    const { requestOpen, onEnter, onExited, children, className } = props;

    useEffect(() => {
        if (requestOpen) {
            onEnter();
        }
    }, [onEnter, requestOpen]);

    const handleAnimationEnd = useCallback(() => {
        if (!requestOpen) {
            onExited();
        }
    }, [onExited, requestOpen]);

    return (
        <div
            onAnimationEnd={handleAnimationEnd}
            className={className + (requestOpen ? ' open' : ' close')}
        >
            {children}
        </div>
    );
}

Animated.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    onEnter: PropTypes.func.isRequired,
    onExited: PropTypes.func.isRequired,
    requestOpen: PropTypes.bool.isRequired,
};
const PopAnimation = styled(Animated)`
  @keyframes open-animation {
    0% {
      opacity: 0;
      transform: translateY(-8px) scale(0.95);
    }

    50% {
      opacity: 1;
      transform: translateY(4px) scale(1.05);
    }

    100% {
      opacity: 1;
      transform: translateY(0) scale(1);
    }
  }

  @keyframes close-animation {
    0% {
      opacity: 1;
      transform: translateY(0) scale(1);
    }

    50% {
      opacity: 1;
      transform: translateY(4px) scale(1.05);
    }

    100% {
      opacity: 0;
      transform: translateY(-8px) scale(0.95);
    }
  }

  &.open {
    animation: open-animation 0.4s ease-in forwards;
  }

  &.close {
    animation: close-animation 0.4s ease-in forwards;
  }
`;
export default PopAnimation;