import { Box, CircularProgress } from "@mui/material";

const Loading = () => {
    return (
        <Box display="flex" width="100%" justifyContent="center"  >
            <CircularProgress  size={20} /> 
        </Box>
    );
}
export default Loading;