import { Unstable_Popup as BasePopup } from '@mui/base/Unstable_Popup';
import { Logout as LogoutIcon } from '@mui/icons-material';
import { useState } from 'react';
import { Box, IconButton, Typography } from '@mui/material';
import PopAnimation from '../utils/PopAnimation';
import { useTranslation } from 'react-i18next';
import CheckButton from '../utils/CheckButton';
import CancelButton from '../utils/CancelButton';
import { clearUserData, useUserDispatch } from '../../context/UserContext';
import OutsideAlerter from '../utils/OutsideAlerter';

const Logout = () => {
    const [anchor, setAnchor] = useState(null);
    const [open, setOpen] = useState(false);
    const dispatch = useUserDispatch()
    const { t } = useTranslation()
    return (
        <OutsideAlerter handleOutClick={() => setOpen(false)}>
            <IconButton ref={setAnchor} onClick={() => setOpen((o) => !o)} sx={{ color: "common.white" }}>
                <LogoutIcon />
            </IconButton>
            <BasePopup style={{ zIndex: 999 }} anchor={anchor} open={open} withTransition placement="bottom-end" >
                {(props) => (
                    <PopAnimation {...props}>
                        <Box bgcolor="text.primaryChannel" sx={{ mt: "20px", p: "20px", borderRadius: "8px" }}  >
                            <Typography color="text.secondaryChannel" variant='h6' >{t("all.logoutAsk")}</Typography>
                            <Box pt="10px" display="flex" alignItems="center" justifyContent="flex-end" >
                                <CheckButton handleClick={() => clearUserData({ dispatch })} />
                                <CancelButton handleClick={() => setOpen(false)} />
                            </Box>
                        </Box>
                    </PopAnimation>
                )}
            </BasePopup>
        </OutsideAlerter>
    );
}
export default Logout