import Chart from "chart.js/auto";
import { CategoryScale } from "chart.js";
import { Doughnut } from "react-chartjs-2"
import { useState } from "react";

Chart.register(CategoryScale);

const Circle = () => {

    const [chartData, setChartData] = useState({
        labels: ['Blue', 'black'],
        datasets: [
            {
                label: 'My First Dataset',
                data: [40, 60],
                borderWidth: 0,
                backgroundColor: ['#34B3F1', '#292D30'],
                // hoverOffset: 40
            },
        ],
    });

    return (
        <Doughnut
            data={chartData}
            options={{
                cutout: "63%",
                layout: {
                    padding: 20
                },
                plugins: {
                    labelCenter: {
                        labels: [
                            {
                                text: 'Activities',
                            },
                        ],
                    },
                    legend: {
                        align: 'center',
                        position: 'bottom',
                        labels: {
                            filter: (legendItem, data) =>
                                typeof legendItem.text !== 'undefined',
                            boxWidth: 16,
                            boxHeight: 16,
                            useBorderRadius: true,
                            borderRadius: 4,
                            padding: 40,
                            font: {
                                size: 14,
                                weight: 400,
                                color: '#8D9092',
                            },
                        },
                    },
                },
            }}
        />
    );
};

export default Circle;
