import { Box, IconButton, Popover } from "@mui/material";
import CloseIcon from '@mui/icons-material/Close';

const PopUp = ({ setAnchorEl, anchorEl, children }) => {
  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  return (
    <Popover
      // className={classes.root}
      sx={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        "& .MuiPaper-root": {
          borderRadius: "10px",
          backgroundColor: "primary.dark",
          boxShadow: "none",
          overflow: "hidden"
        },
      }}
      id={id}
      open={open}
      anchorReference={"none"}
      onClose={handleClose}
    >
      <Box display="flex" justifyContent="flex-end" >
        <IconButton onClick={handleClose}><CloseIcon fontSize="large" sx={{ color: "common.white" }} /></IconButton>
      </Box>
      <Box sx={{
        overflowY: "auto",
        margin: "0px 10px 10px 10px",
        maxWidth: "70vw",
        maxHeight: "700px"
      }} >
        {children}
      </Box>
    </Popover>
  );
};

export default PopUp;
