import { SnackbarProvider, } from 'notistack';

export default function CustomSnackbar({ children }) {

  return (
    <SnackbarProvider autoHideDuration={2000}>
      {children}
    </SnackbarProvider>
  );
}