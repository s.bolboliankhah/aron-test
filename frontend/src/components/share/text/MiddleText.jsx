import { Typography, } from "@mui/material";

const MiddleText = ({ text }) => {
    return (
        <Typography
            component="h4"
            variant='h4'
            color="background.default"
            sx={{
                textAlign: "center",
            }}
        >
            {text}
        </Typography>
    );
}
export default MiddleText;