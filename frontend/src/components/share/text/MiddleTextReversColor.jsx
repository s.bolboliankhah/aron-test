import { Typography, } from "@mui/material";

const MiddleTextReversColor = ({ text }) => {
    return (
        <Typography
            component="h4"
            variant='h4'
            color="text.primaryChannel"
            sx={{
                textAlign: "center",
            }}
        >
            {text}
        </Typography>
    );
}
export default MiddleTextReversColor;