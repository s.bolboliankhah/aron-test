import { Box, Typography, } from "@mui/material";
import { useTranslation } from "react-i18next";

const NotFoundText = () => {
    const { t } = useTranslation()
    return (
        <Box display="flex" width="100%" justifyContent="center" mt="20%"  >
            <Typography color="text.primaryChannel" variant="h4" >{t("all.noData")}</Typography>
        </Box>
    );
}
export default NotFoundText;