import { Typography, } from "@mui/material";

const SmallText = ({ text }) => {
    return (
        <Typography
            component="h6"
            variant='body1'
            color="background.default"
            sx={{
                textAlign: "center",
            }}
        >
            {text}
        </Typography>
    );
}
export default SmallText;