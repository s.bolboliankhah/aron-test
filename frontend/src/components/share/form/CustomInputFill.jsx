import { TextField } from "@mui/material";
import { useField } from "formik";
import { styled } from "@mui/material/styles";
import { useFormikContext } from "formik";
// import PropTypes from "prop-types";

export const TextFieldCustom = styled(TextField)(({ theme }) => ({
  margin: "15px 0px",
  "& input[type=number]": {
    MozAppearance: "textfield",
  },
  "& input[type=number]::-webkit-outer-spin-button": {
    WebkitAppearance: "none",
    margin: 0,
  },
  "& input[type=number]::-webkit-inner-spin-button": {
    WebkitAppearance: "none",
    margin: 0,
  },
  "& .MuiFormLabel-root": {
    // right: "20px !important",
    // left: "auto !important",
    // transformOrigin: "top right !important",
  },
  "& input": {
    "&:-webkit-autofill": {
      WebkitBoxShadow: `0 0 0px 1000px ${theme.palette.common.white} inset !important`,
      webkitTextFillColor: theme.palette.common.white,
    },
  },
  "& .MuiFilledInput-root": {
    backgroundColor: theme.palette.common.white,
    border: "solid #D1D1D6 1px",
    borderRadius: "8px",
  },
  "& .MuiFilledInput-input": {
    "&:-webkit-autofill": {
      backgroundColor: theme.palette.common.white,
      borderRadius: "8px",
    },
  },
  "& .MuiFilledInput-input": {
    "&:focus": {
      backgroundColor: theme.palette.common.white,
      borderRadius: "8px",
    },
    "&:hover": {
      backgroundColor: theme.palette.common.white,
      borderRadius: "8px",
    },
  },
  "& .MuiFilledInput-input": {
    "& .MuiFilledInput-input.Mui-disabled": {
      backgroundColor: theme.palette.common.white,
    },
  },
  "& .MuiInputBase-input": {
    backgroundColor: theme.palette.common.white,
    "& .MuiFilledInput-input": {
      backgroundColor: theme.palette.common.white,
    },
  },

}));
const CustomInputFill = ({
  name,
  disabled,
  text,
  placeholder = "",
  ...otherProps
}) => {
  const [field, meta] = useField(name);
  const { setFieldValue } = useFormikContext();
  field.value = field.value || "";

  const handleChange = (event) => {
    setFieldValue(name, event.target.value);
  };

  const configTextField = {
    ...field,
    ...otherProps,
    disabled: !!disabled,
    fullWidth: true,
    variant: "filled",
    label: text,
    placeholder,
    onChange: handleChange,
  };
  if (meta && meta.error && meta.touched) {
    configTextField.error = true;
    configTextField.helperText = meta.error;
  }
  return (
    <TextFieldCustom
      InputProps={{
        disableUnderline: true,

        sx: {
          bgcolor: "white",
          backgroundColor: "white"

        }
      }}

      {...configTextField}
    />
  );
};

export default CustomInputFill;
