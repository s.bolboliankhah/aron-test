import { Button, Typography } from "@mui/material";
import { useFormikContext } from "formik";
import Loading from '../Loading';

const SubmitButtonFill = ({ text, isLoading, ...otherProps }) => {
  const { submitForm, errors } = useFormikContext();

  const handleSubmit = () => {
    submitForm();
  };

  const configButton = {
    ...otherProps,
    variant: "contained",
    fullWidth: true,
    disabled: !!isLoading,
    onClick: handleSubmit,
  };
  return (
    <Button
      color="primary"
      sx={{
        height: { sm: "56px", xs: "45px" },
        color: "common.white",
        boxShadow: "0 0.5rem 1.5rem 0.5rem rgba(0, 0, 0, 0.075)",
        "&:hover": {
          bgcolor: "primary.main",
        },
        "&.MuiButton-root": {
          borderRadius: "20px",
        },
      }}
      {...configButton}
    >
      {!isLoading ? (
        <Typography
          variant='h6'
          color="common.white"
        >
          {text}
        </Typography>
      ) : (
        <Loading />
      )}
    </Button>
  );
};

export default SubmitButtonFill;
