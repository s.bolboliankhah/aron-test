import { Box, Grid, Typography } from "@mui/material";
import { useNavigate } from "react-router-dom";
import BackButton from "../utils/BackButton";

const GoBack = () => {
    const navigate = useNavigate()
    return (
        <Box display="flex" alignItems="center"  >
            <BackButton handleClick={() => navigate(-1)} />
            <Typography variant="h5" px={2} color="text.primaryChannel" >Back</Typography>
        </Box>
    );
}
export default GoBack;