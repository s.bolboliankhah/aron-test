import axios from 'axios';
import { removeStorageData } from "../context/UserContext"

export const BaseUrlV1 = "http://127.0.0.1:4444/api/v1/";

const Axios = axios.create({
  baseURL: BaseUrlV1,
  headers: {
    "Content-Type": "application/json",
    "Accept": "application/json"
  }
});

export default async function http({ method, url, data }) {
  try {
    const response = await Axios.request({
      method,
      url,
      data,
    });
    if (response.status === 200) {
      return {
        success: true,
        messages: [{ message: response.data.message }],
        dataBody: response.data,
        variant: "success"
      };
    }
    return {
      success: false,
      messages: response.data.errors,
      dataBody: {},
      variant: "error"
    };
  } catch (error) {
    // console.log('=------error.response----', error.response?.data?.errors);
    const status = error.response?.status;

    if (status === 401) {
      return removeStorageData()
    }
    return {
      success: false,
      messages: error.response?.data?.errors || [{ message: "Something is Wrong" }],
      dataBody: {},
      variant: "error"
    };
  }
}
