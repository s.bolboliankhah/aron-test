import { Box, Container } from "@mui/material";
import CustomSnackbar from "../../components/share/CustomSnackbar";
import Register from "../../components/register";

const Index = () => {
  return (
    <CustomSnackbar>
      <Box display="grid" width="100%" height="99vh" alignItems="center" justifyContent="center" >
        <Register />
      </Box>
    </CustomSnackbar>
  );
};

export default Index;
