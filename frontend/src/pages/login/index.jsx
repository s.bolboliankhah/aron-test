import { Box, Container } from "@mui/material";
import Login from "../../components/login";
import CustomSnackbar from "../../components/share/CustomSnackbar";

const Index = () => {
  return (
    <CustomSnackbar>
      <Box display="grid" width="100%" height="99vh" alignItems="center" justifyContent="center" >
        <Login />
      </Box>
    </CustomSnackbar>
  );
};

export default Index;
