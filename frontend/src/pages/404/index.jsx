import { useTranslation } from "react-i18next";

const NotFound = () => {
  const { t, i18n: { language } } = useTranslation();

  return <h1>{t("all.notFound")}</h1>
}

export default NotFound
