# React.js Project README

## Getting Started

### Prerequisites

Before running the React.js project, ensure that the backend is up and running. Follow the steps below to set up:

1. Start the backend server by navigating to [http://localhost:4444](http://localhost:4444) in your web browser. Confirm that you receive the "Root Aron" text to verify the backend is functioning correctly.

### Installation

To install the necessary dependencies for the React.js project, run the following commands:
### `npm install`


If you are using a Linux operating system, execute:
### `npm run linux`

Runs the app in the development mode.\
Open [http://localhost:3006](http://localhost:3006) to view it in your browser.


For Windows operating systems, use:
### `npm run win`

Runs the app in the development mode.\
Open [http://localhost:3006](http://localhost:3006) to view it in your browser.


### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### OPT Code
 your OTP code is : `11111`

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)