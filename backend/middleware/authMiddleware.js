"use strict";
const { Helper } = require("../components/helper");
const { userStatus } = require("../components/Enums");
const response = require("../middleware/responseHandler");

class authMiddleware {

  static async user(req, res, next) {
    if (
      req.headers &&
      req.headers.authorization &&
      req.headers.authorization.split(" ")[0].toLocaleLowerCase() === "bearer"
    ) {
      try {
        const user = await Helper.TokenVerify(
          req.headers.authorization.split(" ")[1],
          "user"
        );
        if (user.status != userStatus[2])
          return response.customError(
            res,
            res.t(user.status, { scope: "Auth" }),
            401
          );
        req.user = user;
        next();
      } catch (error) {
        req.user = undefined;
        return response.customError(res, res.t("401", { scope: "Auth" }), 401);
      }
    } else {
      req.user = undefined;
      return response.customError(res, res.t("401", { scope: "Auth" }), 401);
    }
  }
}

module.exports = authMiddleware;
