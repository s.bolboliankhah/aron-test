"use strict";
const _ = require("lodash");
const fs = require("fs");
var path = require("path");

class ResponseHandler {

  static customError(res, message, code, folder, lastFile) {
    if (folder && lastFile) {
      fs.unlink(
        path.join(process.cwd() + `/public/files/${folder}/${lastFile}`),
        (err) => {
          return res.status(code).json({ errors: [{ message }] });
        }
      );
    } else {
      return res.status(code).json({ errors: [{ message }] });
    }
  }
  static success(res, data, message) {
    if (!Array.isArray(data)) {
      data = JSON.parse(JSON.stringify(data));
    } else {
      data = { docs: data };
    }
    return res.status(200).json({
      ...data,
      message,
    });
  }
  static validation(res, errors, field = "all") {
    if (_.isString(errors)) {
      return res.status(400).json({
        errors: [
          {
            field,
            message: errors,
          },
        ],
      });
    }
    //e.type && e.context
    let list = [];
    if (errors.details[0]) {
      // console.log(e);
      for (let i in errors.details) {
        let e = errors.details[i];
        let name = e.type.includes("array") ? e.context.label : e.context.key;
        name =
          e.path.length > 2
            ? typeof e.path[e.path.length - 2] === "number"
              ? name
              : e.path[e.path.length - 2]
            : name;
        list.push({
          message: res.t(e.type, {
            scope: "joi",
            name: res.t(name, { scope: "joi.field" }),
            valids: e.context.valids,
            limit: e.context.limit?.display
              ? res.t(e.context.limit.display, { scope: "joi.field" })
              : e.context.limit,
          }),
          field: e.path.toString(),
        });
      }
      return res.status(400).json({
        errors: list,
      });
    } else {
      return res.status(400).json({
        errors: [
          {
            message: errors.message,
            field: errors.path.toString(),
          },
        ],
      });
    }
  }
  static catchError(res, err) {
    console.log("********************************");
    console.log(err);
    console.log("********************************");

    ResponseHandler.customError(
      res,
      res.t("Internall", { scope: "Server" }),
      500,
      { err }
    );
    throw err;
  }
  
}

module.exports = ResponseHandler;
