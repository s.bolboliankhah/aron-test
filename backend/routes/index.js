var express = require("express")
var router = express.Router();


router.use('/api/v1', require('./v1/index'));

/* GET home page. */
router.use('/', (req, res) => {
    res.send('root Aron')
});

module.exports = router;
