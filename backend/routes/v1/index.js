const express = require('express');
const router = express.Router();
const rateLimit = require('express-rate-limit');
const localize = require('../../components/lang/index');

// // another limiter
// const defaultLimiter = rateLimit({
// 	windowMs: 10 * 60 * 1000, // 10 min
// 	max: 1000, // limit each IP to 1000 requests per windowMs
// 	message:  {
// 		errors: [{
// 			field: 'all',
// 			message: localize.translate('Auth.429')
// 		}]
// 	}
// });

const authLimiter = rateLimit({
	windowMs: 5 * 60 * 1000, // 5 minutes
	max: 10, // limit each IP to 10 requests per windowMs
	message: {
		errors: [{
			field: 'all',
			message: localize.translate('Auth.429')
		}]
	}
});

router.use('/auth', authLimiter, require('./auth'));

module.exports = router;
