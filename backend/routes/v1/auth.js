var express = require("express")
var router = express.Router();
const { LoginController, RegisterController } = require("../../controllers/v1/auth")

router.post('/login', LoginController.Login);
router.post('/register', RegisterController.Register);
router.post('/verify-otp', RegisterController.VerifyOTP);
router.post('/resend-otp', RegisterController.reSendCode);
// router.post('/set-pass', RegisterController.SetPassword);


module.exports = router;

