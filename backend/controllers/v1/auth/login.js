"use strict";
const joi = require("joi");
const { User } = require("../../../db");
const response = require("../../../middleware/responseHandler");
const { Helper } = require("../../../components/helper");
const { userStatus } = require("../../../components/Enums");

class LoginController {

  static async Login(req, res) {
    try {
      const schema = joi.object().keys({
        mobile: joi
          .string()
          .custom(Helper.mobile, "mobile validation")
          .required(),
        password: joi.string().required(),
      });
      const { error, value } = schema.validate(req.body, { abortEarly: false });
      if (error) {
        return response.validation(res, error);
      }
      const user = await User.findOne({ mobile: value.mobile });

      if (!user)
        return response.validation(res, res.t("Auth.WrongUserPass"));

      if (user.status != userStatus[1])
        return response.customError(
          res,
          res.t(user.status, { scope: "Auth" }),
          403
        );

      let compare = await Helper.Compare(value.password, user.password);
      if (!compare)
        return response.validation(res, res.t("Auth.WrongUserPass"));

      const token = await Helper.GenerateToken(user, "user");
      delete user["_doc"]["password"];

      return response.success(res, { user, token }, res.t("CRUD.Success"));
    } catch (error) {
      return response.catchError(res, error);
    }
  }
  
}

module.exports = LoginController;
