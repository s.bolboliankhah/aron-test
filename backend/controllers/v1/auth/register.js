"use strict";
const joi = require("joi");
const { User } = require("../../../db");
const response = require("../../../middleware/responseHandler");
const { Helper } = require("../../../components/helper");
const { userStatus } = require("../../../components/Enums");

class RegisterController {

  static async Register(req, res) {
    try {
      const schema = joi.object().keys({
        fname: joi
          .string()
          .trim()
          .min(1)
          .max(70)
          .custom(Helper.persian, "persian string")
          .required(),
        lname: joi
          .string()
          .trim()
          .min(1)
          .max(70)
          .custom(Helper.persian, "persian string")
          .required(),
        mobile: joi
          .string()
          .custom(Helper.mobile, "mobile validation")
          .required(),
        email: joi.string().max(70).email().required(),
        password: joi
          .string()
          .custom(Helper.password, "password validation")
          .min(5)
          .max(15)
          .required(),
        repeatPassword: joi.string().valid(joi.ref("password")),
      });
      const { error, value } = schema.validate(req.body, { abortEarly: false });
      if (error) {
        return response.validation(res, error);
      }

      const timeOTP = Number(process.env.OTP_USER_EXPIRE);
      const key = process.env.KEY_OTP_USER;

      const remainingSecond = await Helper.IsGetOTP(
        key + value.mobile,
        timeOTP
      );
      if (!!remainingSecond)
        return response.success(
          res,
          { page: "userOTP", remainingSecond },
          res.t("Auth.WaitTwoMin", { second: remainingSecond })
        );

      const duplicate = await User.findOne({ mobile: value.mobile }).select([
        "status",
      ]);
      if (duplicate) {
        if (duplicate.status != userStatus[0]) {
          return response.validation(res, res.t("Auth." + duplicate.status));
        }
      } else {
        value.status = userStatus[0];
        value.password = await Helper.Hash(value.password);
        const user = new User(value);
        await user.save();
      }

      // const code = await Helper.RandomCode();
      const code = "11111"
      await Helper.StoreOTP(key + value.mobile, timeOTP, code);
      // const done = await SendSMS(value.mobile, code, "registerUserOTP");
      // if (!done) return response.validation(res, res.t("CRUD.ErrorSendOTP"));
      console.log("code: ", code);

      return response.success(
        res,
        { page: "userOTP", remainingSecond: timeOTP },
        res.t("Auth.SentCode")
      );
    } catch (error) {
      return response.catchError(res, error);
    }
  }
  static async VerifyOTP(req, res) {
    try {
      const schema = joi.object().keys({
        mobile: joi
          .string()
          .custom(Helper.mobile, "mobile validation")
          .required(),
        code: joi.string().length(5).regex(/^\d+$/).required(),
      });
      const { error, value } = schema.validate(req.body, { abortEarly: false });
      if (error) {
        return response.validation(res, error);
      }

      const user = await User.findOne({ mobile: value.mobile }).select([
        "status",
        "fname",
        "lname",
        "mobile"
      ]);
      if (!user)
        return response.customError(
          res,
          res.t("CRUD.NotFound", { name: res.t("joi.field.user") }),
          404
        );
      if (user.status != userStatus[0])
        return response.validation(res, res.t("Auth." + user.status));

      const errorCompare = await Helper.CompareOTP(
        process.env.KEY_OTP_USER + value.mobile,
        value.code
      );

      if (errorCompare)
        return response.validation(res, res.t(errorCompare), "code");

      await user.updateOne({ status: userStatus[1] });

      const token = await Helper.GenerateToken(user, "user");

      return response.success(
        res,
        { user, token },
        res.t("Auth.SetPass")
      );
    } catch (error) {
      return response.catchError(res, error);
    }
  }
  static async reSendCode(req, res) {
    try {
      const schema = joi.object().keys({
        mobile: joi
          .string()
          .custom(Helper.mobile, "mobile validation")
          .required(),
      });
      const { error, value } = schema.validate(req.body, { abortEarly: false });
      if (error) {
        return response.validation(res, error);
      }

      const timeOTP = Number(process.env.OTP_USER_EXPIRE);
      const key = process.env.KEY_OTP_USER;

      const remainingSecond = await Helper.IsGetOTP(
        key + value.mobile,
        timeOTP
      );
      if (!!remainingSecond)
        return response.success(
          res,
          { page: "userOTP", remainingSecond },
          res.t("Auth.WaitTwoMin", { second: remainingSecond })
        );

      const user = await User.findOne({ mobile: value.mobile }).select([
        "status",
      ]);
      if (!user) {
        return response.customError(
          res,
          res.t("CRUD.NotFound", { name: res.t("joi.field.user") }),
          404
        );
      }

      if (user.status != userStatus[0]) {
        return response.validation(res, res.t("Auth." + user.status));
      }

      // const code = await Helper.RandomCode();
      const code = "11111"
      await Helper.StoreOTP(key + value.mobile, timeOTP, code);
      // const done = await SendSMS(value.mobile, code, "registerUserOTP");
      // if (!done) return response.validation(res, res.t("CRUD.ErrorSendOTP"));
      console.log("code: ", code);

      return response.success(
        res,
        { page: "userOTP", remainingSecond: timeOTP },
        res.t("Auth.SentCode")
      );
    } catch (error) {
      return response.catchError(res, error);
    }
  }

}

module.exports = RegisterController;
