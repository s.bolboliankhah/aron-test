const mongoose = require("mongoose");
// const { Logger } = require("mongodb");
const { MONGO_USERNAME, MONGO_PASSWORD, MONGO_HOSTNAME, MONGO_PORT, MONGO_DB } =
  process.env;

const options = {
  // useCreateIndex: true,
  // useFindAndModify: false,
};

const url = MONGO_USERNAME
  ? `mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}`
  : `mongodb://${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}`;

mongoose.set("strictQuery", false);
mongoose
  .connect(url, options)
  .then(function () {
    console.log("MongoDB is connected");
    // Logger.setLevel("debug");
    // Logger.filter("class", ["Db"]);
  })
  .catch(function (err) {
    console.log("========", err);
    console.error(`Error db : ${err.message}`);
    process.exit(1);
  });
