var createError = require("http-errors");
var express = require("express");
require("dotenv").config();
var app = express();
const server = require("http").createServer(app);
const toobusy = require("toobusy-js");

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  next();
});

app.use((req, res, next) => {
  if (toobusy()) {
    // log if you see necessary
    res.status(503).json({ errors: [{ message: "Server Too Busy" }] });
  } else {
    next();
  }
});

//db config
require("./config/db");

// setup redis
redis = require("ioredis");
client = redis.createClient();
client.on("connect", function () {
  console.log("Redis client connected");
});
client.on("error", function (err) {
  console.log("\n\n\nSomething went wrong \n\n\n" + err);
});

const cors = require("cors");
// Then pass them to cors:
app.use(cors());

const bodyParser = require("body-parser");
app.use(bodyParser.json({ limit: "1mb" }));
app.use(bodyParser.urlencoded({ extended: true, limit: "1mb" }));

app.set("trust proxy", 1); // trust first proxy

app.use((err, req, res, next) => {
  if (err) {
    console.log("=========", err);
    res.status(400).send({ errors: [{ message: "error parsing data" }] });
  } else {
    next();
  }
});

//setup Localization
require("./components/lang/index");
const localizify = require("localizify");
app.use((req, res, next) => {
  const lang = localizify.default.detectLocale(req.headers["accept-language"]) || "fa";
  localizify.default.setLocale(lang);
  res.t = localizify.t;
  next();
});

app.use("/", require("./routes/index"));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

let PORT = process.env.PORT;
server.listen(PORT, () => {
  console.log(`REST server started on ${PORT}`);
});

module.exports = app;
