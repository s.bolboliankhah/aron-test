# Express.js Project README

## Getting Started

### Prerequisites

1. Before running the Express.js project, make sure you have MongoDB installed. Follow the steps below to set up the backend:

2. Copy the contents of the `.env.example` file to a new file named `.env` in the same directory.


### Installation
To install the necessary dependencies for the Express.js project, run the following commands:
### `npm install`


### Running
### `npm run dev`
Start the backend server by navigating to [http://localhost:4444](http://localhost:4444) in your web browser. Confirm that you receive the "Root Aron" text to verify the backend is functioning correctly.


