module.exports = {
  apps : [{
    name: "Aron",
    script: "./app.js",
    instances: "3",
    env: {
      NODE_ENV: "production",
    },
    env_development : {
      NODE_ENV : "development",
    },
    env_production: {
      NODE_ENV: "production",
    }
  }]
}
