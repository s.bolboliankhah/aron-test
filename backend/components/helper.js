"use strict";
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const redis = require("./redis");
const { USER_KEY } = process.env;
const redisDataUserTime = Number(process.env.DATA_USER_EXPIRE) || 36000; //10 min
const preKey = process.env.PRE_TOKEN || "";
class Helper {


  //token
  static async TokenVerify(token, type) {
    try {
      let key;
      switch (type) {
        case "user":
          key = USER_KEY;
          break;
        default:
          throw false;
      }
      const decode = await jwt.verify(token, key);
      const user = await redis.Get(preKey + type + "-" + decode.sub);
      if (!user || user.token != token) throw false;
      delete user.token;
      return user;
    } catch (e) {
      throw false;
    }
  }
  static async GenerateToken(user, type) {
    let key = "error";
    let data = {};
    switch (type) {
      case "user":
        key = USER_KEY;
        data = {
          lastLoginAt: null,
          token: null,
          _id: user._id,
          fname: user.fname,
          lname: user.lname,
          email: user.email,
          mobile: user.mobile,
          status: user.status,
        };
        break;
      default:
        break;
    }
    const token = jwt.sign({ _type: type }, key, {
      subject: user._id + "",
      expiresIn: "10h",
    });
    const lastLoginToken = jwt.decode(token);
    //set lastLogin to new token iat so other tokens will deactivate
    user.lastLoginAt = new Date(lastLoginToken.iat * 1000);
    data.lastLoginAt = user.lastLoginAt;
    data.token = token;
    await user.save();
    await redis.Set(
      preKey + type + "-" + data._id,
      data,
      "EX",
      redisDataUserTime
    );
    return token;
  }

  //custom validation joi
  static persian(value, helpers) {
    const a2p = (s) =>
      s.replace(/[٠-٩]/g, (d) => "۰۱۲۳۴۵۶۷۸۹"["٠١٢٣٤٥٦٧٨٩".indexOf(d)]);
    const e2p = (s) => s.replace(/\d/g, (d) => "۰۱۲۳۴۵۶۷۸۹"[d]);
    value = a2p(value);
    value = e2p(value);
    const re = /^[\u0600-\u06FF\u0698\u067E\u0686\u06AF ]+$/;
    if (re.test(value)) return value;
    return helpers.error("any.invalidString");
  }
  static mobile(value, helpers) {
    const valid = Helper.checkFormatMobile(value, null);
    if (valid) return valid;
    return helpers.error("any.invalidMobile");
  }
  static checkFormatMobile(value, helpers) {
    const mobile = value.slice(-10);
    const re = /^9([0-9]{9}$)/;
    if (re.test(mobile)) return "98" + mobile;
    if (helpers) return value;
    return false;
  }
  static password(value, helpers) {
    const re =
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d~`!@#$%^&*()_+=-|?><,.}{":';]{5,}$/;
    if (re.test(value)) return value;
    return helpers.error("any.passwordError");
  }

  //password
  static async Hash(password) {
    const salt = await bcrypt.genSalt(10);
    return await bcrypt.hash(password, salt);
  }
  static async Compare(password, Hash) {
    return await bcrypt.compare(password, Hash);
  }

  //  otp
  static RandomCode() {
    return (Math.ceil(Math.random() * 89999) + 10000).toString();
  }
  static async IsGetOTP(key, time) {
    const data = await redis.Get(key);
    if (data && data.sent) {
      const diff = new Date().getTime() - new Date(data.sent).getTime();
      const second = Math.floor(Math.abs(diff) / 1000);
      return second > 0 ? time - second : 0;
    }
    return 0;
  }
  static async StoreOTP(key, time, code = "", data = {}) {
    const now = new Date();
    return await redis.Set(key, { sent: now, code, ...data }, "EX", time);
  }
  static async CompareOTP(key, code) {
    const data = await redis.Get(key);
    if (data && data.code) {
      if (data.code != code) return "Auth.InvalidCode";
      await redis.Delete(key);
      return null;
    }
    return "Auth.ExpireCode";
  }

  // hash key
  static async GenerateHashKey(username, data = {}) {
    const crypto = require("crypto");
    const time = Number(process.env.HASH_KEY_EXPIRE);
    const key = process.env.HASH_KEY;
    const hashKey = crypto
      .createHash("md5")
      .update(key + new Date())
      .digest("hex");
    await redis.Set(key + hashKey, { username, ...data }, "EX", time);
    return hashKey;
  }
  static async GetHashKeyData(hashKey) {
    const key = process.env.HASH_KEY;
    const data = await redis.Get(key + hashKey);
    if (data && data.username) return data;
    return {};
  }

}
module.exports = { Helper };
