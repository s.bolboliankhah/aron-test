const localizify = require('localizify');

const en = require('./messages/en.json');
const fa = require('./messages/fa.json');

const localize = localizify.default
    .add('en', en)
    .add('fa', fa)
    .setLocale('en');

module.exports = localize