const userStatus = ["waiting", "verified", "blocked"];

module.exports = {
  userStatus,
};
