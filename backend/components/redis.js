"use strict";
class redis {
    async Set(key, value, ...args) {
        return await client.set(key, JSON.stringify(value), ...args);
    }
    async Replace(key, value) {
        return await client.multi().del(key).set(key, JSON.stringify(value)).exec();
    }
    async Get(key) {
        return JSON.parse(await client.get(key));
    }
    async Delete(key) {
        return await client.del(key);
    }
    async has(key) {
        return await client.exists(key);
    }
}

module.exports = new redis();