const mongoose = require("mongoose");
const { userStatus } = require("../components/Enums");
// const mongooseDelete = require("mongoose-delete");

const User = new mongoose.Schema(
  {
    fname: String,
    lname: String,
    mobile: String,
    email: String,
    password: String,
    image: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "File",
    },
    lastLoginAt: Date,
    status: {
      type: String,
      default: userStatus[0],
    },
    lastStatus: {
      type: String,
      default: null,
    },
  },
  { timestamps: true }
);
// User.plugin(mongooseDelete, { deletedAt: true, overrideMethods: "all" });

exports.User = mongoose.model("User", User);
